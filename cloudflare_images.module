<?php

/**
 * @file
 * Implements hooks for the cloudflare_images module.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_file_url_alter().
 */
function cloudflare_images_file_url_alter(&$uri) {
  $config = \Drupal::config('cloudflare_images.settings');
  $requestHost = \Drupal::request()->getHost();
  $siteHost = $config->get('site_host');
  $siteName = $config->get('site_name');
  $cloudflareHash = $config->get('cloudflare_hash');

  if ($requestHost !== $siteHost) {
    return;
  }

  $imagePath = _cloudflare_images_path_from_uri($uri);
  if (!$imagePath) {
    return;
  }

  $uri = "https://imagedelivery.net/$cloudflareHash/$siteName/$imagePath/public";
}

/**
 * Implements hook_entity_presave().
 */
function cloudflare_images_entity_presave(EntityInterface $entity) {
  drupal_register_shutdown_function('_cloudflare_images_entity', $entity, 'update');
}

/**
 * Implements hook_entity_presave().
 */
function cloudflare_images_entity_predelete(EntityInterface $entity) {
  drupal_register_shutdown_function('_cloudflare_images_entity', $entity, 'delete');
}

/**
 * {@inheritdoc}
 */
function _cloudflare_images_entity(EntityInterface $entity, $action) {
  $config = \Drupal::config('cloudflare_images.settings');
  $requestHost = \Drupal::request()->getHost();
  $requestSchemeAndHttpHost = \Drupal::request()->getSchemeAndHttpHost();
  $siteHost = $config->get('site_host');
  $siteName = $config->get('site_name');
  $cloudflareAccount = $config->get('cloudflare_account');
  $cloudflareToken = $config->get('cloudflare_token');

  if ($requestHost !== $siteHost) {
    return;
  }

  $bundles = ['image'];
  if (in_array($entity->bundle(), $bundles)) {
    $fileUri = $entity->field_media_image->entity->getFileUri();
    $imagePath = _cloudflare_images_path_from_uri($fileUri);
    if (!$imagePath) {
      return;
    }
    $imageUrl = "$requestSchemeAndHttpHost/$imagePath";
    $cloudflareImageId = "$siteName/$imagePath";

    try {
      \Drupal::httpClient()->post(
        "https://api.cloudflare.com/client/v4/accounts/$cloudflareAccount/images/v1",
        [
          'headers' => [
            'Authorization' => 'Bearer ' . $cloudflareToken,
          ],
          'multipart' => [
            [
              'name' => 'url',
              'contents' => $imageUrl,
            ],
            [
              'name' => 'id',
              'contents' => $cloudflareImageId,
            ],
            [
              'name' => 'metadata',
              'contents' => json_encode([
                'site_uuid' => \Drupal::config('system.site')->get('uuid'),
                'site_name' => $siteName,
                'media_uuid' => $entity->uuid(),
                'file_uuid' => $entity->get('field_media_image')->entity->uuid(),
                'image_url' => $imageUrl,
                'user_uuid' => \Drupal::currentUser()->id(),
              ]),
            ],
          ],
        ],
      );
    }
    catch (RequestException $e) {
      \Drupal::logger('cloudflare_images')->error($e->getMessage());
    }
  }
}

/**
 * {@inheritdoc}
 */
function _cloudflare_images_path_from_uri($uri) {
  // @todo extract from media configuration.
  $mediaExtensions = ['png', 'gif', 'jpg', 'jpeg'];
  /** @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager */
  $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
  $scheme = $stream_wrapper_manager::getScheme($uri);
  $pathinfo = pathinfo($uri);

  if (!in_array($scheme, ['public'])) {
    return;
  }

  if (!in_array($pathinfo['extension'], $mediaExtensions)) {
    return;
  }

  $wrapper = $stream_wrapper_manager->getViaScheme($scheme);
  $path = $wrapper->getDirectoryPath() . '/' . $stream_wrapper_manager::getTarget($uri);

  if (str_contains($path, 'styles')) {
    return;
  }

  return $path;
}
