<?php

namespace Drupal\cloudflare_images\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Remix settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_images_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cloudflare_images.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['settings']['site_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal host to enable functionality'),
      '#default_value' => $this->config('cloudflare_images.settings')->get('site_host'),
      '#description' => $this->t('Used to only push images on live stage. Example: graphabase.pantheonsite.io'),
    ];

    $form['settings']['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sitename to use as prefix when generating image URLs'),
      '#default_value' => $this->config('cloudflare_images.settings')->get('site_name'),
      '#description' => $this->t('Used to namespace your images. Example: graphabase'),
    ];

    $form['settings']['cloudflare_account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloudflare Account ID'),
      '#default_value' => $this->config('cloudflare_images.settings')->get('cloudflare_account'),
    ];

    $form['settings']['cloudflare_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloudflare Account Hash'),
      '#default_value' => $this->config('cloudflare_images.settings')->get('cloudflare_hash'),
    ];

    $form['settings']['cloudflare_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloudflare Token'),
      '#default_value' => $this->config('cloudflare_images.settings')->get('cloudflare_token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cloudflare_images.settings')
      ->set('site_host', $form_state->getValue('site_host'))
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('cloudflare_account', $form_state->getValue('cloudflare_account'))
      ->set('cloudflare_hash', $form_state->getValue('cloudflare_hash'))
      ->set('cloudflare_token', $form_state->getValue('cloudflare_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
